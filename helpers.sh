#!/bin/bash
if [ "$1" = '-?' ];then
  echo "$(basename $0) - Image and container info helpers."
  exit
fi  

repo=
repo_tag=
repo_name=
cont_name=
img_id=
cont_id=

function has_docker {
  ret=`docker -v`
  if [ "${ret:0:6}" = 'Docker' ]; then
    echo 1
    exit
  fi  
  echo 0  
}

function has_podman {
  ret=`podman -v`
  if [ "${ret:0:6}" = 'podman' ]; then
    echo 1
    exit
  fi  
  echo 0  
}

function executable {
  if [ $(has_podman) = 1 ];then
    echo 'podman'
  else
    echo 'docker'
  fi  
}

function get_cache {
  local base="$1"
  if [ ! -d "${base}vars" ]; then
    mkdir ${base}vars
  else
    if [ -f "${base}vars/REPO_TAG" ]; then
      repo_tag=`cat ${base}vars/REPO_TAG`
    fi  
    if [ -f "${base}vars/REPO" ]; then
      repo=`cat ${base}vars/REPO`
    fi  
    if [ -f "${base}vars/REPO_NAME" ]; then
      repo_name=`cat ${base}vars/REPO_NAME`
    fi  
    if [ -f "${base}vars/IMG_ID" ]; then
      img_id=`cat ${base}vars/IMG_ID`
    fi  
    if [ -f "${base}vars/CONT_ID" ]; then
      cont_id=`cat ${base}vars/CONT_ID`
    fi  
    if [ -f "${base}vars/CONT_NAME" ]; then
      cont_name=`cat ${base}vars/CONT_NAME`
    fi  
  fi  
}

function set_cache {
  local base="$1/"
  local repo="$2"
  echo "$repo" > ${base}vars/REPO
  repo_tag=`echo $repo | awk -F ':' '{print $3}'`
  echo "$repo_tag" > ${base}vars/REPO_TAG
  repo_name=`echo $repo | awk -F ':' '{print$1 ":" $2}'`
  echo "$repo_name" > ${base}vars/REPO_NAME
  echo "New stickey image name = '$repo'"
  if [ -f ${base}vars/COND_ID ]; then
    unlink ${base}vars/CONT_ID
  fi
  if [ -f ${base}vars/COND_NAME ]; then
    unlink ${base}vars/CONT_NAME
  fi
  if [ -f ${base}vars/IMG_ID ]; then
    unlink ${base}vars/IMG_ID
  fi
}
