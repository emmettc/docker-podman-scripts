Clone to a directory (which you must have already done :-) and add that directory to your path.

From the command run plist to see a list of avaiable scritps with a brief description.

Run any script with -h to see detailed usage descripton.

Most script create a vars subdirectory in you source directory, so yu will want to add vars/ to your .git/info/exclude file for each source directory you will be running these scripts from.

Using the -h option, note that those scripts that use the vars directory have a -i option.  Once you have used that option, running any of the other scripts that use the vars directory will discover the image name and so you won't need to use it again except to change the docker repo name or version.

The pap script is the "do all" script.  Running it will build and possibly commit and push the newly build rimage.  The -h option is your friend.

