#!/bin/bash
if [ "$1" = '-?' ];then
  echo "$(basename $0) -  Run container"
  exit
fi  
. helpers.sh

if [ $(has_podman) = 1 ];then
  podman='podman'
else
  podman='docker'
fi  

function usage
  {
  echo "Usage: $(basename $0) [ options ] [ command ]"
  echo
  echo "Run $podman container."
  echo
  echo 'Options:'
  echo ' -h - Prints this usage information.'
  echo
  echo ' -i - Image name.  This will make the image name sticky, so it is'
  echo '      only necessary the first time you run this script.'
  echo '      You must include a tag, i.e., ":1.0.1".'
  echo
  echo ' -N - Explicit image name.'
  echo
  echo ' -c - Command. Run this command and ignore command line.'
  echo
  echo " -d - Don't detach contaner, instead run command and exit."
  echo
  echo ' -v - More info output.'
  echo 
  echo ' -n - Dry run'
  echo
  echo 'Command must be quoted if it contains space characters.'
  echo
  exit 1
  }
  

base=`pwd`'/'
get_cache "$base"

cmd=
dry=
quiet='true'
detach='--detach'
while getopts "i:c:N:vnhd" opt; do
  case $opt in
    i)
      repo="$OPTARG"
      set_cache "$base" "$repo"
    ;;
    v)
      quiet=
    ;;
    N)
      cont_name="$OPTARG"
      echo "$cont_name" > ${base}vars/CONT_NAME
    ;;
    d)
      detach=
    ;;
    c)
      cmd="$OPTARG"
    ;;
    n)
      dry='true'
    ;;  
    \?)
      echo "Invalid option: -$OPTARG" >&2
      usage
      ;;
    h)
      usage
      ;;
    :)
      echo "Option -$OPTARG requires an argument." >&2
      usage
      ;;
  esac
done
shift $((OPTIND-1))

if [ -n "$repo" ]; then
  if [ -z "$quiet" ]; then
    echo "Current sticky image name = '$repo'"
  fi  
else
  echo "No sticky image name defined, use dbld -i repo_name first."
  ret_code=0
  exit
fi

if [ -z "$cmd" ] && [ $# -eq 1 ]; then
  cmd="$1"
fi

if [ -z "$img_id" ]; then
  images=`$podman images`

  while read line; do
    if [[ "$line" = *"$repo_name"* ]]; then
      img_id=`echo $line | awk '{ print $3 }'`
      echo "$img_id" > ${base}vars/IMG_ID
      break
    fi
  done <<< "$images"
fi

if [ -z "$quiet" ]; then
  echo "img id = $img_id"
  echo "cmd = $cmd"
fi
if [ -n "$img_id" ]; then
  if [ -n "$cont_name" ];then
      cname="--name $cont_name"
  fi  
  if [ -z "$quiet" ]; then
    echo "$podman run $cname $detach $img_id $cmd"
  fi  
  if [ -z "$dry" ]; then
    $podman run $cname $detach $img_id $cmd
    containers=`$podman ps -a`
    if [ -n "$cont_name" ];then
      test="$cont_name"
    else
      test="$repo"
    fi  
    cont_id=
    while read line; do
      if [[ "$line" = *"$test"* ]]; then
        cont_id=`echo $line | awk '{ print $1 }'`
        echo "$cont_id" > ${base}vars/CONT_ID
        break
      fi
    done <<< "$containers"
    if [ -n "$cont_id" ]; then
      ret_code=1
    else
      echo "Failed to create container"
      ret_code=0
    fi  
  else
    echo 'Would have run:'
    echo "   $podman run $cname $detach $img_id $cmd"
  fi
else
  echo "Valid image not found"
fi
