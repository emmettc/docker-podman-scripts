#!/bin/bash
if [ "$1" = '-?' ];then
  echo "$(basename $0) - Execute command in container"
  exit
fi  
. helpers.sh

if [ $(has_podman) = 1 ];then
  podman='podman'
else
  podman='docker'
fi  

function usage
  {
  echo "Usage: $(basename $0) [ options ] [ command ]"
  echo
  echo "Execute command in $podman container"
  echo
  echo 'Options:'
  echo ' -h prints this usage information.'
  echo
  echo ' -v - More info output.'
  echo 
  echo ' -n - Dry run'
  echo
  exit 1
  }

base=`pwd`'/'
repo=
repo_tag=
repo_name=
cont_name=
img_id=
cont_id=
get_cache "$base"

quiet='true'
dry=
tag=
while getopts "vnh" opt; do
  case $opt in
    v)
      quiet=
    ;;
    n)
      dry='true'
    ;;  
    \?)
      echo "Invalid option: -$OPTARG" >&2
      usage
      ;;
    h)
      usage
      ;;
    :)
      echo "Option -$OPTARG requires an argument." >&2
      usage
      ;;
  esac
done
shift $((OPTIND-1))

cmd='/bin/sh'
if [ $# -eq 1 ]; then
  cmd="$1"
fi

if [ -n "$repo" ]; then
  if [ -z "$quiet" ]; then
    echo "Current sticky image name = '$repo'"
  fi  
else
  echo "No sticky image name defined, use dbld -i repo_name first."
  ret_code=0
  exit
fi

if [ -z "$cont_id" ]; then
  images=`$podman ps -a`

  while read line; do
    if [[ "$line" = *"$repo_name"* ]]; then
      cont_id=`echo $line | awk '{ print $1 }'`
      echo "$cont_id" > ${base}vars/CONT_ID
      break
    fi
  done <<< "$images"
  if [ -z "$cont_id" ]; then
    echo "No running container.  Please use pap, prun or pcre -i repo:tag first"
    exit
  fi
fi

if [ -z "$quiet" ]; then
  echo "$podman exec -it $cont_id /bin/sh"
fi

if [ -z "$dry" ]; then
  $podman exec -it "$cont_id" $cmd
else
  echo "Would have run:"
  echo "    $podman exec -it $cont_id /bin/sh"
fi
