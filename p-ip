#!/bin/bash
if [ "$1" = '-?' ];then
  echo "$(basename $0) - Get container IP address."
  exit
fi  
. helpers.sh

if [ $(has_podman) = 1 ];then
  podman='podman'
else
  podman='docker'
fi  

function usage
  {
  echo "Usage: $(basename $0) [ options ] [ image ]"
  echo
  echo "Get $podman container IP address"
  echo
  echo 'Options:'
  echo ' -h prints this usage information.'
  echo
  echo ' -i - Image name.  Using -i option will make the image name sticky.'
  echo
  echo ' -N - Explicit container name'
  echo
  echo ' -v - More info output.'
  echo 
  echo ' -n - Dry run'
  echo
  echo '  When providing an image name, please include a tag, i.e., ":1.0.1".'
  echo
  exit 1
  }

base=`pwd`'/'
repo=
repo_tag=
repo_name=
cont_name=
img_id=
cont_id=
get_cache "$base"

quiet='true'
dry=
tag=
while getopts "i:N:vnh" opt; do
  case $opt in
    i)
      repo="$OPTARG"
      set_cache "$base" "$repo"
    ;;
    v)
      quiet=
    ;;
    N)
      cont_name="$OPTARG"
      echo "$cont_name" > ${base}vars/CONT_NAME
#      cname="-N $cont_name"
    ;;
    n)
      dry='true'
    ;;  
    \?)
      echo "Invalid option: -$OPTARG" >&2
      usage
      ;;
    h)
      usage
      ;;
    :)
      echo "Option -$OPTARG requires an argument." >&2
      usage
      ;;
  esac
done
shift $((OPTIND-1))

if [ -z "$cont_name" ]; then
  echo "No sticky image name defined, use -i option to set sticky image name, or run pbld or ppl with -i opton."
  usage
elif [ -z "$quiet" ]; then
  echo "Container name = $cont_name"
fi

if [ -z "$dry" ]; then
  $podman inspect --format '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' $cont_name
else
  echo "Would have run:"
  echo "   $podman inspect $cont_name to get IP address of container"
fi
